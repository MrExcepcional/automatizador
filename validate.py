#!/usr/local/bin/python

import tkinter as tk
from tkinter import filedialog


def path_correctos(path1, path2):
    # Comprobamos que sean los excel que comiencen con "ExecutedBudget" y
    # "WORK_ORDERS"
    # Comprobamos que sean archivos ".xls"
    correctos = False
    if ".xlsx" in path1 and ".xlsx" in path2:
        # Comprobamos que se parezcan a los nombres objetivos
        if("ExecutedBudget" in path1+path2 and "WORK_ORDERS" in path1+path2):
            correctos = True
    return correctos

# Método que retorna "ExecutedBudget" en primera posición (pathEB)
# y "WORK_ORDERS" en segunda posición (pathWO)


def asignar_paths(p1, p2):
    if "WORK_ORDERS" in p1:
        return p2, p1
    return p1, p2

# Se implementa entrega de archivos mediante argumentos desde consola
# En caso de no encontrar archivos en argumentos, se solicitan mediante
# dialogo de abrir archivo.
# Si el num de argumentos es 3, se presume que son los dos excel necesarios


def run(argumentos):
    pathEB = pathWO = ''
    satisface_nombre = False
    if len(argumentos) == 3:
        # si los nombres de archivo son correctos se asignan
        if path_correctos(argumentos[1], argumentos[2]):
            pathEB, pathWO = asignar_paths(argumentos[1], argumentos[2])
            satisface_nombre = True
        else:
            print("No se entregaron los argumentos esperados (excels con" +
                  " nombres que comiencen con \"ExecutedBudget\" y " +
                  "\"WORK_ORDERS\"). \nSe piden de manera gráfica...")
    # Si no se entregan los argumentos, abrimos diálogo para encontrarlos
    else:
        # TODO: Hacer interfaz gráfica decente con botón para cada archivo
        root = tk.Tk()
        root.withdraw()
        path1 = filedialog.askopenfilename()
        path2 = filedialog.askopenfilename()
        if path_correctos(path1, path2):
            pathEB, pathWO = asignar_paths(path1, path2)
            satisface_nombre = True
        else:
            # Se avisa al usuario que no se encontraron los archivos
            # Se termina el programa
            print(
                "No se encontraron los archivos correctos para procesar." +
                "\nTerminando programa..."
                )
    return satisface_nombre, pathEB, pathWO
