#!/usr/local/bin/python

import pandas as pd

WO_keys = ["OT",  # ID de OT
           ["En Proceso", "Finalizadas", "Cancelado", "En Revisión"],  # Estado
           ["SUPERIOR", "INFERIOR", "MAQUINA"],  # Activo
           ["//", "EA LOGISTICA - 3M"],  # Ubicado en ó es Parte de
           ["Equipos", "Ubicaciones"],  # Tipo de Activo
           ["Preventivo", "Visita Cliente"]  # Tipo de Tarea
           ]
esperadas_en_AEB = ('ID Orden de Trabajo', 'Estado', 'Activo',
                    'Ubicado en ó es Parte de', 'Tipo de Activo',
                    'Tipo de Tarea', 'Fecha de Inicio', 'Responsable')
esperadas_en_BWO = ()


def rectificar(pathEB, pathWO):
    # rectificamos los excels
    """lectura de archivos"""
    dfA = pd.read_excel(pathEB)
    dfB = pd.read_excel(pathWO)
    # Quitamos filas completamente vacías
    dfA = dfA.dropna(how='all')
    dfB = dfB.dropna(how='all')
    # tamaño de cada archivo
    size_WO_B = len(dfB)
    size_EB_A = len(dfA)
    # Ubicamos correctamente los nombres de las columnas
    # (ya que están desfasados por el título)
    dfA.columns = dfA.iloc[0]
    dfB.columns = dfB.iloc[0]
    """
	#Verificamos que están todas las columnas que necesitamos
	AEBenc, AEBfalt = encontrar_columnas(dfA.columns, esperadas_en_AEB)
	if len(AEBfalt)>0: print("¡Ups!, no están todas las columnas que necesitamos")
	BWOenc, BWOfalt = encontrar_columnas(dfB.columns, esperadas_en_BWO)
	if len(BWOfalt)>0: print("¡Ups!, no están todas las columnas que necesitamos")
	"""
    # Ordenamos filas según OT
    dfA = dfA.sort_values(dfA.columns[0])
    dfB = dfB.sort_values(dfB.columns[0])
    # Aseguramos el index correcto, después de ordenar
    dfA.index = range(0, size_EB_A)
    dfB.index = range(0, size_WO_B)
    return dfA, dfB, size_EB_A, size_WO_B


def encontrar_columnas(headers, esperadas):
    """Este método 'encontrar_columnas' no está en uso aún."""
    encontradas = []
    faltan = []
    for c in esperadas:
        if c in headers:
            encontradas.append(headers.index(c))
        else:
            faltan.append(c)
    return encontradas, faltan
