#!/usr/local/bin/python
# -*- coding: latin-1 -*-
"""Script automatización de formato de datos.

Abrimos dos archivos de excel que serán la fuente de datos.

Procesamos las celdas relavantes y las escribimos en RESULTADO.xlsx
"""

# Import de módulos
import sys

import pandas as pd

import validate
import escritor
from rectifier import rectificar
from machinery import Machinery

# Validate.run() nos entrega bool si los path ingresados son válidos y los paths en sí
satisface_nombre, path_eb, path_wo = validate.run(sys.argv)

if satisface_nombre:
    df_a, df_b, SIZE_EB_A, SIZE_WO_B = rectificar(path_eb, path_wo)
    # Columnas de las OTs
    OTB = df_b[df_b.columns[0]]
    OTA = df_a[df_a.columns[0]]

    # Contadores iniciales
    ini_filaA = ini_filaB = 1

    # Instancia de maquinaria
    m = Machinery(df_b, ini_filaB)

    # Llenamos el formato de pathSalida
    dfsalida = pd.DataFrame(escritor.crear_formato_salida())

    # Mientras no se acaba el archivo, llenamos agregamos las filas
    while (ini_filaA < SIZE_EB_A and ini_filaB < SIZE_WO_B):
        # Los acentos generan problemas, por lo que se reemplaza *algunos
        # headers explícitos por df_a(B).columns[x], siendo x la posición
        # del header correspondiente.
        # Cuando una OT fue cancelada, no se agrega ni se procesa.
        if not df_b['Estado'][ini_filaB] == 'Cancelado':
            # ESTE VALOR NO VA EN NINGUNA CELDA. Se utiliza en clasificaciones.
            tipo_activo = df_b['Tipo de Activo'][ini_filaB]
            # CONTROLAR CUANDO CAMBIAN EL ORDEN DE LAS COLUMNAS
            cliente = m.cliente_segun_tipo(tipo_activo)
            fecha_del_servicio = m.control_empty(
                df_b['Fecha de Inicio'][ini_filaB], lambda x: x.split()[0].strip('\''))
            mes = m.control_empty(fecha_del_servicio,
                                  lambda x: int(x.split('-')[1]))
            folio_del_reporte_inicial = df_b[df_b.columns[0]][ini_filaB]
            servicio_a_realizar = "NUEVO"  # valor único "NUEVO"
            n_folio_en_caso_d = ""  # se deja en blanco por instrucción del cliente
            linea_del_cliente = ""  # se deja en blanco por instrucción del cliente
            modelo_serie = df_b['Activo'][ini_filaB]
            maquina, serie, placa, maquina_o_cabezal = m.maquina_breakdown(
                modelo_serie, tipo_activo)
            # Si no existe la OT en ExecutedBudget, se dejan vacíos estos parámetros
            refaccion = descripcion = qty = ""
            if OTB[ini_filaB] == OTA[ini_filaA]:
                refaccion, descripcion = m.refac_breakdown(
                    df_a['Descripcion del Recurso'][ini_filaA])
                qty = df_a['Cantidad'][ini_filaA]
                ini_filaA += 1
            costo = ''  # se deja en blanco por instrucción del cliente
            tipo_de_servicio = df_b['Tipo de Tarea'][ini_filaB]
            qty_svc = 1  # valor único '1'
            causa = tipo_de_servicio  # son el mismo valor duplicado
            # se deja en blanco por instrucción del cliente
            p_firmo_conformidad = ''
            h_entrada = m.control_empty(
                df_b['Fecha de Inicio'][ini_filaB], lambda x: x.split()[1])
            h_salida = m.control_empty(df_b[df_b.columns[18]][ini_filaB],
                                       lambda x: x.split()[1]
                                      ) # df_b.columns[18] == 'Fecha de finalización'
            nombre_del_tecnico_3m = df_b['Responsable'][ini_filaB]
            status_del_servicio = "CERRADO"  # valor único "CERRADO"
            # Se eliminan estas letras y espacios a la derecha de cualquier número
            mach_type = maquina.rstrip('ABCDEFGHIJKLMNOPQRSTUVWXYZ afr')
            grupo = ''  # se deja en blanco por instrucción del cliente
            # Un par de nombres largos
            contin_serv = 'NO DE FOLIO EN CASO DE SER CONTINUIDAD DE SERVICIO'
            p_conform = 'PERSONA QUE FIRMO  DE CONFORMIDAD  (CLIENTE)'
            # Llenamos la fila
            nueva_fila = {'Mes': mes, 'CLIENTE': cliente,
                          'FECHA DEL SERVICIO': fecha_del_servicio,
                          'FOLIO DEL REPORTE INICIAL ': folio_del_reporte_inicial,
                          'SERVICIO A REALIZAR ': servicio_a_realizar,
                          contin_serv: n_folio_en_caso_d,
                          'LINEA DEL CLIENTE': linea_del_cliente,
                          'MAQ. MODELO / SERIE / PLACA': modelo_serie,
                          'MAQUINA': maquina, 'SERIE': serie, 'PLACA': placa,
                          'REFACCION': refaccion, 'DESCRIPCION': descripcion,
                          'QTY': qty,
                          'COSTO USD': costo,
                          'TIPO SERVICIO': tipo_de_servicio,
                          'QTY SVC': qty_svc, 'CAUSA ': causa,
                          p_conform: p_firmo_conformidad,
                          'HORA DE ENTRADA  TECNICO  3M': h_entrada,
                          'HORA DE SALIDA  TECNICO  3M': h_salida,
                          'NOMBRE DEL TECNICO 3M': nombre_del_tecnico_3m,
                          'STATUS DEL SERVICIO': status_del_servicio,
                          'MAQUINA O CABEZAL': maquina_o_cabezal,
                          'MACH TYPE': mach_type, 'Grupo': grupo, 'mes': mes}
            # Agregamos la nueva fila al dataframe
            dfsalida = dfsalida.append(nueva_fila, ignore_index=True)
            # Asumimos que solo en ExecutedBudget se repiten OTS y que en
            # WORKORDERS las OTS son únicas.
            while (ini_filaA >1 and ini_filaA < SIZE_EB_A and
                   df_a['Id OT'][ini_filaA-1] == df_a['Id OT'][ini_filaA]):
                refaccion, descripcion = m.refac_breakdown(
                    df_a['Descripcion del Recurso'][ini_filaA])
                qty = df_a['Cantidad'][ini_filaA]
                nueva_fila['REFACCION'] = refaccion
                nueva_fila['DESCRIPCION'] = descripcion
                nueva_fila['QTY'] = qty
                # Agregamos la fila con los nuevos datos
                dfsalida = dfsalida.append(nueva_fila, ignore_index=True)
                ini_filaA += 1
        ini_filaB += 1
        #Actualizamos también el índice de machinery
        m.update_iniB(ini_filaB)

    # Guardamos el archivo de dfsalida
    escritor.guardar(dfsalida, "RESULTADO.xlsx")
