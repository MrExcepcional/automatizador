import unittest
import pandas as pd


class OutputValidationTest(unittest.TestCase):
	"""docstring for OutputValidationTest"""
	

	def setUp(self):
		self.MODELO = pd.read_excel('MODELO.xlsx')
		self.RESULTADO = pd.read_excel('RESULTADO.xlsx')

	def test_result_equal_model(self):
		hint_message = "RESULTADO debería ser igual al MODELO"
		self.assertTrue(self.MODELO.equals(self.RESULTADO), hint_message)



if __name__ == '__main__':
	unittest.main()