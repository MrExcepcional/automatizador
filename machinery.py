#!/usr/local/bin/python

import re

import pandas as pd

# Función para clasificar si es máquina, cabezal o nada (visita a ubicación)
def clasificar_m_c(activo, tipo_activo):
    if tipo_activo != 'Ubicaciones':
        if not pd.isna(activo):
            if 'CAB' in activo:
                return 'CABEZAL'
            return 'MAQUINA'
    return 'Ubicaciones'


class Machinery():
    """docstring for Machinery"""

    def __init__(self, dfB, ini_filaB):
        self.dfB = dfB
        self.ini_filaB = ini_filaB

    def update_iniB(self, index):
        self.ini_filaB = index

    # función para controlar celdas vacías
    def control_empty(self, valor, funcion):
        if pd.isna(valor) or valor == '':
            o_t = self.dfB[self.dfB.columns[0]][self.ini_filaB]
            print("Se encontro un valor vacio en " + o_t)
            return ''
        return funcion(valor)

    # Método para separar refacción y descripción
    def refac_breakdown(self, texto):
        refac = desc = ''
        if not pd.isna(texto):
            partes = re.split('[{}]', texto)
            if len(partes) > 1:
                refac = partes[1].strip()
            desc = partes[0].strip("0123456789 ")
        return refac, desc

    # Método para separar modelo (== máquina), serie y placa de un Activo
    def maquina_breakdown(self, activo, tipo_activo):
        tipo = clasificar_m_c(activo, tipo_activo)
        partes = re.split('[{}]', activo)
        placa = serie = modelo = ''
        if len(partes) > 1:
            # Queremos que nos clasifique para MAQUINA y CABEZAL. No para Ubicaciones
            placa = partes[1].strip()
            if not tipo == 'Ubicaciones':
                partes = partes[0].split()
                if tipo == 'CABEZAL':
                    modelo = partes[-1]
                else:  # entonces es máquina: lleva serie y modelo
                    serie = partes[-1]
                    modelo = partes[-2]
            else:
                modelo = partes[0]
        return modelo, serie, placa, tipo

    # Función para buscar nombre del cliente en 'Activo' cuando el tipo es 'Ubicaciones'
    def cliente_segun_tipo(self, tipo):
        resultado = ''
        if tipo == 'Ubicaciones':
            resultado = self.dfB['Activo'][self.ini_filaB].split('{')[0].strip()
        else:  # 'Ubicado en ó es Parte de' == dfB.columns[5]
            resultado = self.dfB[self.dfB.columns[5]][self.ini_filaB]
            if len(resultado.split('/')) > 4:
                resultado = resultado.split('/')[4].strip()
            else:
                o_t = self.dfB[self.dfB.columns[0]][self.ini_filaB]
                print('ADVERTENCIA: no se encontró nombre del cliente en ' + o_t)
        return resultado
