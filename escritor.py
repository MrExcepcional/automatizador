#!/usr/local/bin/python

from pandas import ExcelWriter


def guardar(dfsalida, path_salida):
    writer = ExcelWriter(path_salida, engine='xlsxwriter')
    dfsalida.to_excel(writer, 'Sheet1', index=False)
    worksheet = writer.sheets['Sheet1']
    worksheet.set_column('A:A', 4)
    worksheet.set_column('B:B', 24)
    worksheet.set_column('C:E', 12)
    worksheet.set_column('F:G', 2)
    worksheet.set_column('H:J', 9)
    worksheet.set_column('L:M', 15)
    worksheet.set_column('N:N', 4)
    worksheet.set_column('O:O', 2)
    worksheet.set_column('P:P', 9)
    worksheet.set_column('Q:Q', 4)
    worksheet.set_column('R:R', 9)
    worksheet.set_column('S:S', 2)
    worksheet.set_column('Z:Z', 2)
    worksheet.set_column('AA:AA', 4)
    writer.save()

def crear_formato_salida():
    return {"Mes": [], "CLIENTE": [], "FECHA DEL SERVICIO": [],
            "FOLIO DEL REPORTE INICIAL ": [], "SERVICIO A REALIZAR ": [],
            "NO DE FOLIO EN CASO DE SER CONTINUIDAD DE SERVICIO": [],
            "LINEA DEL CLIENTE": [], "MAQ. MODELO / SERIE / PLACA": [],
            "MAQUINA": [], "SERIE": [], "PLACA": [], "REFACCION": [],
            "DESCRIPCION": [], "QTY": [], "COSTO USD": [], "TIPO SERVICIO": [],
            "QTY SVC": [], "CAUSA ": [],
            "PERSONA QUE FIRMO  DE CONFORMIDAD  (CLIENTE)": [],
            "HORA DE ENTRADA  TECNICO  3M": [],
            "HORA DE SALIDA  TECNICO  3M": [], "NOMBRE DEL TECNICO 3M": [],
            "STATUS DEL SERVICIO": [], "MAQUINA O CABEZAL": [], "MACH TYPE": [],
            "Grupo": [], "mes": [], "CLIENTE": []}
